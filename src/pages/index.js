import { withTheme } from "emotion-theming";
import { Hero } from "../components/Hero";
import { Container } from "../components/Container";
import { DarkModeSwitch } from "../components/DarkModeSwitch";
    
    import { CTA } from "../components/CTA";
const Index = () => (
  <Container>
    <Hero />
    {/* <Main /> */}
    <DarkModeSwitch />

   
    {/* <Footer>
      <Text>The Hub 360</Text>
    </Footer> */}
    <CTA />
  </Container>
);

export default withTheme(Index);
