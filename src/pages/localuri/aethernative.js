import { withTheme } from "emotion-theming";
import { Container } from "../../components/Container";
import { DarkModeSwitch } from "../../components/DarkModeSwitch";
import { CTA } from "../../components/CTA";
import {TurVirtual} from "../../components/TurVirtual"
const Aethernative = (props) => (
  <Container>
    <TurVirtual url='https://tourmkr.com/F1IX1L8wyg'/>
    {/* <Main /> */}
    <DarkModeSwitch />
    {/* <Footer>
      <Text>The Hub 360</Text>
    </Footer> */}
    <CTA />
  </Container>
);

export default withTheme(Aethernative);
