import NextApp from "next/app";
import { ThemeProvider, CSSReset, ColorModeProvider } from "@chakra-ui/core";
import MyMenu from "../components/MyMenu";
import theme from "../theme";
import { Stack } from "@chakra-ui/core";
import React, { useEffect } from "react";
class App extends NextApp {

 
  render() {
    const { Component, pageProps } = this.props;
  
    return (
      <ThemeProvider theme={theme}>
        <ColorModeProvider>
          <CSSReset />
          <link
            href="https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.css"
            rel="stylesheet"
          />
          <Stack>
            <MyMenu />
            <Component />
          </Stack>
        </ColorModeProvider>
      </ThemeProvider>
    );
  }
}

export default App;
