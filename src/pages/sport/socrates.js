import { withTheme } from "emotion-theming";
import { Container } from "../../components/Container";
import { CTA } from "../../components/CTA";
import {TurVirtual} from "../../components/TurVirtual"
const Socrates = (props) => (
  <Container>
    <TurVirtual url='https://tourmkr.com/F19XDpIy29'/>
    {/* <Main /> */}
    {/* <Footer>
      <Text>The Hub 360</Text>
    </Footer> */}
    <CTA />
  </Container>
);

export default withTheme(Socrates);
