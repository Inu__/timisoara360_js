import { withTheme } from "emotion-theming";
import { Container } from "../../components/Container";
import { DarkModeSwitch } from "../../components/DarkModeSwitch";
import { CTA } from "../../components/CTA";
import {TurVirtual} from "../../components/TurVirtual"
const StradaAlbaIulia = (props) => (
  <Container>
    <TurVirtual url='https://gothru.co/PPdwOroMg?index=scene_2&hlookat=32&vlookat=3&fov=120'/>
    {/* <Main /> */}
    {/* <Footer>
      <Text>The Hub 360</Text>
    </Footer> */}
    <CTA />
  </Container>
);

export default withTheme(StradaAlbaIulia);
