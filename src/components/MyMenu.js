import { initGA, logPageView } from "../hooks/useAnalytics"

import React, { useEffect } from "react";
import {
  Stack,
  useDisclosure,
  LightMode,
  Drawer,
  DrawerOverlay,
  DrawerBody,
  DrawerHeader,
  DrawerContent,
  Button, Text
} from "@chakra-ui/core";
import Link from "next/link";
import Router from "next/router";
import NProgress from "nprogress";
import {UseAnalytics} from '../hooks/useAnalytics'

import { DarkModeSwitch } from "../components/DarkModeSwitch"
Router.onRouteChangeStart = url => NProgress.start();
Router.onRouteChangeComplete = url => NProgress.done();
Router.onRouteChangeError = url => NProgress.done();

export default props => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  useEffect(() => {
    initGA();
    logPageView();
    Router.events.on("routeChangeComplete", () => {
      logPageView();
    });
  }, []);
  return (
    <Stack p={4} position="fixed" zIndex={2}>
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.css"
        integrity="sha256-pMhcV6/TBDtqH9E9PWKgS+P32PVguLG8IipkPyqMtfY="
        crossOrigin="anonymous"
      />
      <LightMode>
        <Button
          icon="view"
          onClick={onOpen}
          variantColor="red"
          variant="solid"
          _focus={{ outline: 0, boxShadow: "outline" }}
        >
          Alege categoria
        </Button>
      </LightMode>
      <Drawer placement="right" onClose={onClose} isOpen={isOpen} size="sm">
        <DrawerOverlay />
        <DrawerContent>
          <DrawerHeader borderBottomWidth="10px" bg="tomato">
            <Text color="white" fontSize={22}>
              Alege Categoria
            </Text> <DarkModeSwitch />
          </DrawerHeader>
          <DrawerBody overflow="scroll">
            <DrawerHeader borderBottomWidth="1px" fontSize={22}>Obiective Turistice</DrawerHeader>
            <Stack spacing={3}>
              <Link href="/obiective/catedrala" >
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Catedrala</Text></a>
              </Link>
              <Link href="/obiective/bastion" >
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Bastion</Text></a>
              </Link>
              <Link href="/parcuri/muzeulsatului" >
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Muzeul Satului</Text></a>
              </Link>
            </Stack>
            <DrawerHeader borderBottomWidth="1px" fontSize={22}>Agrement</DrawerHeader>
            <Stack spacing={3}>
              <Link href="/parcuri/piataunirii">
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Piața Unirii</Text></a>
              </Link>
              <Link href="/parcuri/piatavictoriei">
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Piața Victoriei</Text></a>
              </Link>
              <Link href="/parcuri/parculscudier">
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Parcul Scudier</Text></a>
              </Link>
              <Link href="/parcuri/piatalibertatii">
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Piața Libertății</Text></a>
              </Link>
              <Link href="/parcuri/parculcatedralei" p={5}>
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Parcul Catedralei</Text></a>
              </Link>
              <Link href="/parcuri/parculbotanic" >
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Parcul Botanic</Text></a>
              </Link>
              <Link href="/parcuri/parculflora">
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Parcul Flora</Text></a>
              </Link>

              <Link href="/parcuri/parculuzinei">
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Parcul Uzinei</Text></a>
              </Link>
              <Link href="/parcuri/parculpoporului">
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Parcul Poporului</Text></a>
              </Link>
              <Link href="/parcuri/parculrozelor">
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Parcul Rozelor</Text></a>
              </Link>



            </Stack>
            <DrawerHeader borderBottomWidth="1px" fontSize={22}>Localuri</DrawerHeader>
            <Stack spacing={3}>
              <Link href="/localuri/aethernative" >
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Aethernative</Text></a>
              </Link>
              <Link href="/localuri/vimcaffe" >
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Vim Cafe</Text></a>
              </Link>
            </Stack>
            <DrawerHeader borderBottomWidth="1px" fontSize={22}>Sport</DrawerHeader>
            <Stack spacing={3}>
              <Link href="/sport/arenaaquasport" >
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Arena Aquasport</Text></a>
              </Link>
              <Link href="/sport/socrates" >
                <a><Text fontSize={20} pl={10} pb={1} fontWeight='450'>Socrates</Text></a>
              </Link>
            </Stack>
          </DrawerBody>

        </DrawerContent>
      </Drawer>

      {/* <Menu closeOnSelect="true">
        <MenuButton
          mt={3}
          as={Button}
          variantColor="green"
          _focus={{ outline: 0, boxShadow: "outline" }}
        >
          Ajutor
        </MenuButton>
        <MenuList>
          <MenuItem>
            <Link href="/gallaevents">
              <a>Vezi Cazari</a>
            </Link>
          </MenuItem>
          <MenuItem>
            <Link href="/gallaevents">
              <a>Gaseste un Ghid</a>
            </Link>
          </MenuItem>
          <MenuItem>
            <Link href="/gallaevents">
              <a>Inchiriaza o masina</a>
            </Link>
          </MenuItem>
          <MenuItem>
            <Link href="/gallaevents">
              <a>Planuieste o Excursie</a>
            </Link>
          </MenuItem>
        </MenuList>
      </Menu> */}
    </Stack>
  );
};
