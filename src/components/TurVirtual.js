import { Box } from "@chakra-ui/core";

export const TurVirtual = props => {
  return (
    <Box w="100%" h="100vh">
      <iframe
        title={props.titlu}
        height="100%"
        width="100%"
        allowFullScreen
        src={props.url}
      />
      
    </Box>
  );
};
